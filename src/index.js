import React from 'react';
import ReactDOM from 'react-dom';
import ListPage from './components/ListPage';
import CreatePage from './components/CreatePage';
import DetailPage from './components/DetailPage';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { ApolloProvider, createNetworkInterface, ApolloClient } from 'react-apollo';
import 'tachyons';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const networkInterface = createNetworkInterface({
	// __SIMPLE_API_ENDPOINT__ looks similar to: `https://api.graph.cool/simple/v1/<PROJECT_ID>`
	uri: 'https://api.graph.cool/simple/v1/cj995zm3k6zzn0115c9e5jicc'
});

const client = new ApolloClient({ networkInterface });

ReactDOM.render(
	<ApolloProvider client={client}>
		<Router>
			<div>
				<Route exact path="/" component={ListPage} />
				<Route path="/create" component={CreatePage} />
				<Route path="/post/:id" component={DetailPage} />
			</div>
		</Router>
	</ApolloProvider>,
	document.getElementById('root')
);
registerServiceWorker();
